**Api Test**

This project is created with Java Springboot, Mysql DB.

## Database configuration

Before you start, create a database with this configuration
**database = db_alfacart_test** 
**username = root**
**password = toor**

## Endpoint

### running on port 50001

## EMPLOYEE 
## - PUT
add employee
http://localhost:50001/employee

## - POST
update employee
http://localhost:50001/employee/{id}

## - GET
get all list of employee
http://localhost:50001/employee

## - GET
get employee by id
http://localhost:50001/employee/{id}

## - DELETE
delete employee by id
http://localhost:50001/employee/{id}

## DOGS
## - GET
get all list of dogs
http://localhost:50001/dogs

## - GET
get dog by breed
http://localhost:50001/dogs/{breed}

