package com.alfacart.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
@Entity
@Table(name="employee")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)

public class Employee implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	private String full_name;
	
	private String address;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;
	
	
	private Long role_id;
	
	private Long salary;
	
	
	@JsonIgnore
	@ManyToOne()
    @JoinColumn(name = "role_id", insertable=false, updatable=false)
	private Role role;
	
	@Transient
	private String role_name;

	public Employee() {
    }

	public Employee(Long id, String full_name, String address,Timestamp string,Long role_id,Long salary) {
	    this.id = id;
	    this.full_name = full_name;
	    this.address = address;
	    this.dob = string;
	    this.role_id=role_id;
	    this.salary=salary;
	}
	
	public Role getRole() {
		return role;
	}

	public void setRole(Role roleObj) {
		this.role = roleObj;
	}

	public String getRole_name() {
		if (this.role != null)
			role_name=role.getRole_name();
		return role_name;
	}

	public void setRole_name(String role_name) {
		
		this.role_name = role_name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}

	public Long getSalary() {
		return salary;
	}

	public void setSalary(Long salary) {
		this.salary = salary;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", full_name=" + full_name + ", address=" + address + ", dob=" + dob
				+ ", role_id=" + role_id + ", salary=" + salary + ", role_name=" + role_name + "]";
	}

	
	
}
