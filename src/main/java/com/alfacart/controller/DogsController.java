package com.alfacart.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
public class DogsController {

	@Value("${api.dog.list}")
	private String doglist;
	
	@Value("${api.dog.detail}")
	private String dogdetail;
	
	@SuppressWarnings("unchecked")
	@GetMapping("/dogs")
	public  List<Map<String, Object>> getDogsList() {
	      HttpHeaders headers = new HttpHeaders();
	      RestTemplate restTemplate = new RestTemplate();

	      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	      HttpEntity <String> entity = new HttpEntity<String>(headers);
	      
	      Map<String,Object> mp=restTemplate.exchange(doglist, HttpMethod.GET, entity, Map.class).getBody();
	      List<Map<String, Object>> dogs =new ArrayList<Map<String,Object>>();

 	       for (Map.Entry<String, Object> entry : ((Map<String,Object>) mp.get("message")).entrySet()) {
	 			Map<String,Object> breed =new HashMap<>(); 
	   			breed.put("breed", entry.getKey().toString());
	   			breed.put("sub_breed", entry.getValue());
				
	   			dogs.add(breed);
	       }
	   		
	       
	       return dogs;
	   }
	
	@SuppressWarnings("unchecked")
	@GetMapping("/dogs/{id}")
	public  Map<String, Object> getDogDetail(@PathVariable(value="id") String id) {
	      HttpHeaders headers = new HttpHeaders();
	      RestTemplate restTemplate = new RestTemplate();

	      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	      HttpEntity <String> entity = new HttpEntity<String>(headers);
	      
	      Map<String,Object> mp=restTemplate.exchange(dogdetail+id+"/list", HttpMethod.GET, entity, Map.class).getBody();
	      Map<String,Object> dog= new HashMap<>();
	      dog.put("breed",id);
	      dog.put("sub_breed", mp.get("message"));

	      Map<String,Object> img=restTemplate.exchange(dogdetail+id+"/images", HttpMethod.GET, entity, Map.class).getBody();
	      dog.put("images",img.get("message"));
	   		
	       return dog;
	   }
}
