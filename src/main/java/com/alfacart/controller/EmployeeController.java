package com.alfacart.controller;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alfacart.model.Employee;
import com.alfacart.repository.EmployeeRepository;
import com.alfacart.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.validation.annotation.Validated;
@CrossOrigin(origins = "*")
@RestController
public class EmployeeController {
	
	
@Autowired		
EmployeeRepository employeeRepo; 

@Autowired ObjectMapper objectMapper;

/*
 * Get all employee
 */
@GetMapping("/employee")
@ResponseBody
public ResponseEntity<List<Employee>> getAllEmployee(){
    List<Employee> emp =  employeeRepo.findAll();
    return new ResponseEntity<List<Employee>>(emp, HttpStatus.OK);
}

/*
 * Add employee
 */
@PutMapping("/employee")
@ResponseBody
public Response addEmployee(@Validated @RequestBody Employee employee ) {
	 Response response=new Response();
	 if(employee != null){
		 employeeRepo.save(employee);
			response.setMessage("new employee has successfully been added");
			response.setStatus("succes");
			response.setStatuscode("000");
	 }
	 return response;
     
}

/*
 * Get employee by id
 */
@GetMapping("/employee/{id}")
public Optional<Employee> getEmployeeById(@PathVariable(value="id") Long id) {
	return employeeRepo.findById(id);
}


@RequestMapping(value = "employee/{id}", method = RequestMethod.POST )
public @ResponseBody Response update(@PathVariable Long id, HttpServletRequest request) throws IOException
{	
	Optional<Employee> gb = employeeRepo.findById(id);
	Employee currentVal=new Employee();
	 Response response=new Response();

	currentVal.setAddress(gb.get().getAddress());
	currentVal.setDob(gb.get().getDob());
	currentVal.setFull_name(gb.get().getFull_name());
	currentVal.setRole_id(gb.get().getRole_id());
	currentVal.setSalary(gb.get().getSalary());
	currentVal.setId(gb.get().getId());
		
	Employee updatedEmp = objectMapper.readerForUpdating(currentVal).readValue(request.getReader());	
	if(gb.isPresent()) {	
		employeeRepo.saveAndFlush(updatedEmp);
		response.setMessage("employee with id "+id+" has successfully updated");
		response.setStatus("succes");
		response.setStatuscode("000");
		return response;
	}
	else {
		response.setMessage("cannot update employee with id "+id+" because not found");
		response.setStatus("not found");
		response.setStatuscode("404");
		
		return response;
	}
	

}


@DeleteMapping("/employee/{id}")
public Response deleteEmployee(@PathVariable (value="id") Long id){
	Optional<Employee> emp = employeeRepo.findById(id);
	 Response response=new Response();

	String result = "";
	if(emp.isPresent()) {
		employeeRepo.deleteById(id);
		result="employee with id "+id+" successfully delete";
		
		response.setMessage(result);
		response.setStatus("succes");
		response.setStatuscode("000");
		return response;
	}
	else {
		result = "cannot delete employee with id "+id+" because not found";
		
		response.setMessage(result);
		response.setStatus("not found");
		response.setStatuscode("404");
		
		return response;
	}
}
}
