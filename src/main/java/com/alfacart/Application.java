package com.alfacart;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.alfacart.model.Role;
import com.alfacart.repository.RoleRepository;

@SpringBootApplication
@ComponentScan({"com.alfacart"})
@EntityScan({"com.alfacart"})
@EnableJpaAuditing
@EnableAutoConfiguration
public class Application {
	@Autowired
	private RoleRepository roleRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	InitializingBean sendDatabase() {
	    return () -> {
	        roleRepository.save(new Role("Staff"));
	        roleRepository.save(new Role("Manager"));
	        roleRepository.save(new Role("General Manager"));
	        roleRepository.save(new Role("Vice President"));
	        roleRepository.save(new Role("CEO"));
	        roleRepository.save(new Role("Investor"));
	      };
	   }
}
