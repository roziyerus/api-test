package com.alfacart.response;


public class Response {

	private String message;
	

	private String status;
	
	public String getStatuscode() {
		return statuscode;
	}


	public void setStatuscode(String statuscode) {
		this.statuscode = statuscode;
	}


	private String statuscode;


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


}
